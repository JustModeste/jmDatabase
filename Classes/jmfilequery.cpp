#include "jmfilequery.h"

jmFileQuery::jmFileQuery(QSqlDatabase db, QString filename)
{
    this->db = db;
    this->fileName = filename;
}

bool    jmFileQuery::executeSql() {
    bool    ret = false;
    QFile *f = new QFile(this->fileName);
    f->open(QIODevice::ReadOnly);
    QTextStream *file = new QTextStream(f);

    QString Separator = ";";
    QString	sql = "";

    while (!file->atEnd()) {
        QString str = file->readLine();

        if (str.indexOf("SET TERM") != -1) {
            int pos = str.indexOf(Separator);
            Separator = str.mid(9, pos - 10);
        }
        else {
            sql = sql.trimmed() + " " + str.trimmed();
            if (str.indexOf(Separator) != -1) {
qDebug() << sql;
                ret = ExecuteQuery(sql);
qDebug() << ret;
                sql = "";
            }
        }
    }
    return ret;
}

bool jmFileQuery::ExecuteQuery(QString sql) {
    bool    ret = true;

    if (sql == "") {
        return false;
    }
    QSqlQuery query(this->db);
    qDebug() << this->db;

    if (query.prepare(sql)) {
        qDebug() << "Prepare OK";
        if (!query.exec()) {
            ret = false;
        }
    } else {
        qDebug() << "Can't prepare query: " << sql << ": " << query.lastError().text();
    }
    qDebug() << "Result : " << ret;
    return ret;
}
